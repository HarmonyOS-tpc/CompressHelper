﻿## CompressHelper
图片压缩，压缩Pixelmap，CompressImage
主要通过尺寸压缩和质量压缩，以达到清晰度最优。

<img src="https://gitee.com/openharmony-tpc/CompressHelper/raw/master/gifs/compresshelper.gif" width="50%" height="50%"/>

## 使用方法

### 1、在Ability里面使用
```java
    File newFile = CompressHelper.getDefault(this).compressToFile(oldFile);
```

### 2、也可以自定义属性
```java
    File newFile = new CompressHelper.Builder(this)
        .setMaxWidth(720)  // 默认最大宽度为720
        .setMaxHeight(960) // 默认最大高度为960
        .setQuality(80)    // 默认压缩质量为80
        .setFileName(yourFileName) // 设置你需要修改的文件名
        .setCompressFormat(CompressFormat.JPEG) // 设置默认压缩为jpg格式
        .setDestinationDirectoryPath(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES).getAbsolutePath())
        .build()
        .compressToFile(oldFile);
```

## entry运行要求
通过DevEco studio,并下载openharmonySDK
将项目中的build.gradle文件中dependencies→classpath版本改为对应的版本（即您当前IDE新建项目中所用的版本）

## 集成
```
allprojects{
    repositories{
        mavenCentral()
    }
}
implementation 'io.openharmony.tpc.thirdlib:CompressHelper:1.0.3'
```
## Licenses
```
 Copyright 2017 nanchen(刘世麟)

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
```