package com.nanchen.compresshelper;

import ohos.app.Context;
import ohos.media.image.PixelMap;
import ohos.media.image.common.ImageFormat;
import ohos.media.image.common.PixelFormat;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import static ohos.app.Environment.DIRECTORY_PICTURES;

/**
 * 压缩方法工具类
 * <p>
 * Author: nanchen
 * Email: liushilin520@foxmail.com
 * Date: 2017-03-08  9:03
 */

public class CompressHelper {
    private static volatile CompressHelper INSTANCE;
    private Context context;
    /**
     * 最大宽度，默认为720
     */
    private float maxWidth = 720.0f;
    /**
     * 最大高度,默认为960
     */
    private float maxHeight = 960.0f;
    /**
     * 默认压缩后的方式为JPEG
     */
    private ImageFormat.ComponentType compressFormat = ImageFormat.ComponentType.JPEG;
    /**
     * 默认的图片处理方式是ARGB_8888
     */
    private PixelFormat bitmapConfig = PixelFormat.ARGB_8888;
    /**
     * 默认压缩质量为80
     */
    private int quality = 80;
    /**
     * 存储路径
     */
    private String destinationDirectoryPath;
    /**
     * 文件名前缀
     */
    private String fileNamePrefix;
    /**
     * 文件名
     */
    private String fileName;

    public static CompressHelper getDefault(Context context) {
        if (INSTANCE == null) {
            synchronized (CompressHelper.class) {
                if (INSTANCE == null) {
                    INSTANCE = new CompressHelper(context);
                }
            }
        }
        return INSTANCE;
    }

    private CompressHelper(Context context) {
        this.context = context;
        destinationDirectoryPath = context.getExternalFilesDir(DIRECTORY_PICTURES).getAbsolutePath();
    }

    /**
     * 压缩成文件
     *
     * @param file 原始文件
     * @return 压缩后的文件
     */
    public File compressToFile(File file) {
        try {
            return BitmapUtil.compressImage(context, file.getCanonicalPath(), maxWidth, maxHeight,
                    compressFormat, bitmapConfig,
                    quality, destinationDirectoryPath,
                    fileNamePrefix, fileName);
        } catch (IOException ex) {
            Logger.getLogger(CompressHelper.class.getName()).log(Level.SEVERE, ex.getMessage());
            return null;
        }
    }

    /**
     * 压缩为Bitmap
     *
     * @param file 原始文件
     * @return 压缩后的Bitmap
     */
    public PixelMap compressToBitmap(File file) {
        try {
            return BitmapUtil.getScaledPixelmap(context, file.getCanonicalPath(), maxWidth, maxHeight, bitmapConfig);
        } catch (IOException ex) {
            Logger.getLogger(CompressHelper.class.getName()).log(Level.SEVERE, ex.getMessage());
            return null;
        }
    }


    /**
     * 采用建造者模式，设置Builder
     */
    public static class Builder {
        private CompressHelper mCompressHelper;

        public Builder(Context context) {
            mCompressHelper = new CompressHelper(context);
        }

        /**
         * 设置图片最大宽度
         *
         * @param maxWidth 最大宽度
         * @return Builder
         */
        public Builder setMaxWidth(float maxWidth) {
            mCompressHelper.maxWidth = maxWidth;
            return this;
        }

        /**
         * 设置图片最大高度
         *
         * @param maxHeight 最大高度
         * @return Builder
         */
        public Builder setMaxHeight(float maxHeight) {
            mCompressHelper.maxHeight = maxHeight;
            return this;
        }

        /**
         * 设置压缩的后缀格式
         *
         * @param compressFormat compressFormat
         * @return Builder
         */
        public Builder setCompressFormat(ImageFormat.ComponentType compressFormat) {
            mCompressHelper.compressFormat = compressFormat;
            return this;
        }

        /**
         * 设置Bitmap的参数
         *
         * @param bitmapConfig bitmapConfig
         * @return Builder
         */
        public Builder setBitmapConfig(PixelFormat bitmapConfig) {
            mCompressHelper.bitmapConfig = bitmapConfig;
            return this;
        }

        /**
         * 设置压缩质量，建议80
         *
         * @param quality 压缩质量，[0,100]
         * @return Builder
         */
        public Builder setQuality(int quality) {
            mCompressHelper.quality = quality;
            return this;
        }

        /**
         * 设置目的存储路径
         *
         * @param destinationDirectoryPath 目的路径
         * @return Builder
         */
        public Builder setDestinationDirectoryPath(String destinationDirectoryPath) {
            mCompressHelper.destinationDirectoryPath = destinationDirectoryPath;
            return this;
        }

        /**
         * 设置文件前缀
         *
         * @param prefix 前缀
         * @return Builder
         */
        public Builder setFileNamePrefix(String prefix) {
            mCompressHelper.fileNamePrefix = prefix;
            return this;
        }

        /**
         * 设置文件名称
         *
         * @param fileName 文件名
         * @return Builder
         */
        public Builder setFileName(String fileName) {
            mCompressHelper.fileName = fileName;
            return this;
        }

        public CompressHelper build() {
            return mCompressHelper;
        }
    }
}
