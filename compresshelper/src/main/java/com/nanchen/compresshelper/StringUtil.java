package com.nanchen.compresshelper;

/**
 * 字符串相关工具类
 * <p>
 * Author: nanchen
 * Email: liushilin520@foxmail.com
 * Date: 2017-02-19  9:52
 */
public class StringUtil {
    private StringUtil() {
        throw new UnsupportedOperationException("u can't instantiate me...");
    }

    /**
     * 判断字符串是否为null或长度为0
     *
     * @param ch 待校验字符串
     * @return {@code true}: 空<br> {@code false}: 不为空
     */
    public static boolean isEmpty(CharSequence ch) {
        return ch == null || ch.length() == 0;
    }

    /**
     * 判断字符串是否为null或全为空格
     *
     * @param str 待校验字符串
     * @return {@code true}: null或全空格<br> {@code false}: 不为null且不全空格
     */
    public static boolean isSpace(String str) {
        return (str == null || str.trim().length() == 0);
    }

    /**
     * 判断两字符串是否相等
     *
     * @param acs 待校验字符串a
     * @param bcs 待校验字符串b
     * @return {@code true}: 相等<br>{@code false}: 不相等
     */
    public static boolean equals(CharSequence acs, CharSequence bcs) {
        if (acs == bcs) {
            return true;
        }
        int length;
        if (acs != null && bcs != null && (length = acs.length()) == bcs.length()) {
            if (acs instanceof String && bcs instanceof String) {
                return acs.equals(bcs);
            } else {
                for (int i = 0; i < length; i++) {
                    if (acs.charAt(i) != bcs.charAt(i)) {
                        return false;
                    }
                }
                return true;
            }
        }
        return false;
    }

    /**
     * null转为长度为0的字符串
     *
     * @param str 待转字符串
     * @return s为null转为长度为0字符串，否则不改变
     */
    public static String null2Length0(String str) {
        return str == null ? "" : str;
    }

    /**
     * 返回字符串长度
     *
     * @param cs 字符串
     * @return null返回0，其他返回自身长度
     */
    public static int length(CharSequence cs) {
        return cs == null ? 0 : cs.length();
    }

    /**
     * 首字母大写
     *
     * @param str 待转字符串
     * @return 首字母大写字符串
     */
    public static String upperFirstLetter(String str) {
        if (isEmpty(str) || !Character.isLowerCase(str.charAt(0))) {
            return str;
        }
        return String.valueOf((char) (str.charAt(0) - 32)) + str.substring(1);
    }

    /**
     * 首字母小写
     *
     * @param str 待转字符串
     * @return 首字母小写字符串
     */
    public static String lowerFirstLetter(String str) {
        if (isEmpty(str) || !Character.isUpperCase(str.charAt(0))) {
            return str;
        }
        return String.valueOf((char) (str.charAt(0) + 32)) + str.substring(1);
    }

    /**
     * 反转字符串
     *
     * @param str 待反转字符串
     * @return 反转字符串
     */
    public static String reverse(String str) {
        int len = length(str);
        if (len <= 1) {
            return str;
        }
        int mid = len >> 1;
        char[] chars = str.toCharArray();
        char ch;
        for (int i = 0; i < mid; ++i) {
            ch = chars[i];
            chars[i] = chars[len - i - 1];
            chars[len - i - 1] = ch;
        }
        return new String(chars);
    }

    /**
     * 转化为半角字符
     *
     * @param str 待转字符串
     * @return 半角字符串
     */
    public static String toDBC(String str) {
        if (isEmpty(str)) {
            return str;
        }
        char[] chars = str.toCharArray();
        for (int i = 0, len = chars.length; i < len; i++) {
            if (chars[i] == 12288) {
                chars[i] = ' ';
            } else if (65281 <= chars[i] && chars[i] <= 65374) {
                chars[i] = (char) (chars[i] - 65248);
            } else {
                chars[i] = chars[i];
            }
        }
        return new String(chars);
    }

    /**
     * 转化为全角字符
     *
     * @param str 待转字符串
     * @return 全角字符串
     */
    public static String toSBC(String str) {
        if (isEmpty(str)) {
            return str;
        }
        char[] chars = str.toCharArray();
        for (int i = 0, len = chars.length; i < len; i++) {
            if (chars[i] == ' ') {
                chars[i] = (char) 12288;
            } else if (33 <= chars[i] && chars[i] <= 126) {
                chars[i] = (char) (chars[i] + 65248);
            } else {
                chars[i] = chars[i];
            }
        }
        return new String(chars);
    }
}
